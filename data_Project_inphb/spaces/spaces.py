# In each model, we specify the parameters that must be used to drive them, for example this spaces below :

spaces = {
    "GradientBoostingRegressor": {
        'n_estimators': hp.choice('n_estimators', range(30, 150, 30)),
        'max_depth': hp.choice('max_depth', range(1, 6, 2)),
        'learning_rate': hp.uniform('learning_rate', 0.1, 0.5),
        'min_samples_leaf': hp.choice('min_samples_leaf', range(10, 20, 5))
    },
    "RandomForestRegressor": {
        'n_estimators': hp.choice('n_estimators', range(50, 250, 50)),
        'max_depth': hp.choice('max_depth', range(1, 10, 2)),
        'min_samples_leaf': hp.choice('min_samples_leaf', range(10, 20, 5))
    },
    "AdaBoostRegressor": {
        "n_estimators": hp.choice('n_estimators', range(50, 250, 50)),
        "learning_rate": hp.uniform('learning_rate', 0.2, 1),
        "loss": 'linear'
    },
    "xgboost": {
        'max_depth': hp.quniform('max_depth', 1, 30, 1),
        'learning_rate': hp.quniform('learning_rate', 0.01, 0.25, 0.01),
        'n_estimators': hp.choice('n_estimators', range(100, 3000, 5)),
        'silent': True,
        'nthread': 16,
        'gamma': hp.quniform('gamma', 0, 2, 0.1),
        'min_child_weight': hp.quniform('min_child_weight', 1, 10, 1),
        'max_delta_step': hp.quniform('max_delta_step', 0, 10, 1)
    },
    "DecisionTreeRegressor": {
        'max_depth': hp.choice('max_depth', range(1, 4)),
        'min_samples_leaf': hp.choice('min_samples_leaf', range(10, 50, 5)),
        'min_samples_split': hp.choice('min_samples_split', range(20, 50, 5))
    }

}
