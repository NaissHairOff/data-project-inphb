def nb_of_h_f_per_class(dataSet):
    """Number of men and woman per class"""
    import matplotlib.pyplot as plt
    import seaborn as sns
    
    plt.figure(figsize=(8, 6))
    sns.barplot(x = dataSet.Survived, 
            y = dataSet.index,
            hue = dataSet.Sex)
    return plt.show()


def nb_of_h_f_per_Pclass_1(dataSet):
    """Number of men and woman per Pclass"""
    import matplotlib.pyplot as plt
    import seaborn as sns
    
    plt.figure(figsize=(8, 6))
    sns.barplot(x = dataSet.Survived, 
            y = dataSet.index,
            hue = dataSet.Pclass)
    return plt.show()

def nb_of_h_f_per_Pclass_2(dataSet):  
    import matplotlib.pyplot as plt
    import seaborn as sns  
    plt.figure(figsize=(8, 6))
    sns.barplot(x = dataSet.Pclass, 
            y = dataSet.index,
            hue = dataSet.Survived)
    return plt.show()

def nb_of_h_f_per_Embarked(dataSet):
    """Number of men and woman per Embarked"""
    import matplotlib.pyplot as plt
    import seaborn as sns
    
    plt.figure(figsize=(8, 6))
    sns.barplot(y = dataSet.index, 
            x = dataSet.Survived,
            hue = dataSet.Embarked)
    return plt.show()


def nb_of_h_f(dataSet):
    """Number of men and woman"""
    import matplotlib.pyplot as plt
    import seaborn as sns
    
    plt.figure(figsize=(8, 6))
    sns.countplot(x = dataSet.Sex, 
              order = dataSet.Sex.value_counts().index)
    return plt.show()

def prize_of_ticket_per_SibSp(dataSet):
    """The prize of the ticket par SibSp"""
    import matplotlib.pyplot as plt
    import seaborn as sns
    
    plt.figure(figsize=(9, 9))
    sns.scatterplot(x = dataSet.Fare, 
                y = dataSet.SibSp,
                hue = dataSet.Sex, 
                size = dataSet.index, 
                sizes = (20, 200))
    return plt.show()

def prize_of_ticket_per_Age(dataSet):
    """The prize of the ticket par Age"""
    import matplotlib.pyplot as plt
    import seaborn as sns
    
    plt.figure(figsize=(9, 9))
    sns.scatterplot(x = dataSet.Fare, 
                y = dataSet.Age,
                hue = dataSet.Sex, 
                size = dataSet.index, 
                sizes = (20, 200))
    return plt.show()

def more_analyse_1(dataSet):
    """The"""
    import matplotlib.pyplot as plt
    import seaborn as sns
    
    plt.figure(figsize =(9, 9))
    pal1 = dict(male="#ff7315", female="#8cba51")
    g = sns.FacetGrid(dataSet, col="Parch", row = "Survived", 
                      hue="Sex", height=3, aspect=1, palette=pal1)
    g = g.map(plt.scatter, "Age" , "Fare", edgecolor="w", s=70)
    g.add_legend()
    return plt.show()