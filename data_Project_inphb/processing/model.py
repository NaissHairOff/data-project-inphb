def parse_model_1(X, use_columns):
    """Subdivise the data set in Features that defined in parameter and Target"""
    if "Survived" not in X.columns:
        raise ValueError("target column survived should belong to df")
    target = X["Survived"]
    X = X[use_columns]
    return X, target


def split_in_train_and_test_set(X, Y, testSize = 0.2, randomState = 0):
    """Split the data set in train X, Y and test X, Y"""
    from sklearn.model_selection import train_test_split
    return train_test_split(X, Y, test_size = testSize, random_state = randomState)
    
def compute_score(clf, X, Y, CV = 5):
    """compute score in a classification modelisation.
       clf : classifier
       X : features
       y : target
    """
    
    from sklearn.model_selection import cross_val_score 
    
    xval = cross_val_score(clf, X, Y, cv = CV)
    print("Accuracy [Logistic Regression with cv = %d] : %0.2f (+/- %0.2f)" % (CV, xval.mean(), xval.std() * 2))

def model_1_quick_logisticRegression(X, Y):
    from sklearn.linear_model import LogisticRegression
    lr = LogisticRegression()
    compute_score(lr, X, Y)

def model_2_logisticRegression(X_train, Y_train, X_test, Y_test, save_model = False):
    """Make Logistic Regression on a data X_train and Y_train"""
    import pandas as pd
    from sklearn.linear_model import LogisticRegression
    from sklearn import metrics
    lr = LogisticRegression()

    model = lr.fit(X_train, Y_train)
    
    Y_pred = model.predict(X_test)
    #Y_prob = model.predict_proba(X_test)[:,1]
    
    score_train = model.score(X_train, Y_train)
    score_test = model.score(X_test, Y_test)  
    
    metric = metrics.classification_report(Y_test, Y_pred)
    
    """df = pd.DataFrame({
        'Y_test' : Y_test, 
        'Prediction' : Y_pred, 
        'Proba' : Y_prob
    })"""

    #We will need to serialize both the model and the encoders used to create them
    if save_model == True:
        import joblib

        filePath = '../data_Project_inphb/models_saved/logisticRegression_'
        fileName = 'md_2_bkup_.joblib'

        with open(filePath + fileName, 'wb') as f:
            joblib.dump(model, f)
    
    return {"score_train": score_train, "score_test": score_test,
           "model": model, "metric": print(metric)}


def model_3_quick_logisticRegression(X, Y, save_model = False):
    from sklearn.linear_model import LogisticRegression
    lr = LogisticRegression()
    lr.fit(X, Y)
    lr.coef_

    #We will need to serialize both the model and the encoders used to create them
    if save_model == True:
        import joblib

        filePath = '../data_Project_inphb/models_saved/logisticRegression_'
        fileName = 'md_3_bkup_.joblib'

        with open(filePath + fileName, 'wb') as f:
            joblib.dump(lr, f)
    
    compute_score(lr, X, Y)    
    

def parse_model_2(X):
    import pandas as pd
    
    if "Survived" not in X.columns:
        raise ValueError("target column survived should belong to df")
    target = X["Survived"]
    to_dummy = ['Pclass', 'Sex']
    for dum in to_dummy:
        split_temp = pd.get_dummies(X[dum], prefix = dum)
        for col in split_temp:
            X[col] = split_temp[col]
        del X[dum]
    X['Age'] = X['Age'].fillna(X['Age'].median())
    to_del = ["PassengerId", "Name", "Cabin", "Embarked", "Survived", "Ticket"]
    for col in to_del:
        del X[col]
    return X, target


def model_4_quick_logisticRegression(X, Y, save_model = False):
    from sklearn.linear_model import LogisticRegression
    import pandas as pd
    
    lr = LogisticRegression()
    
    compute_score(lr, X, Y)
    
    lr.fit(X, Y)

    #We will need to serialize both the model and the encoders used to create them
    if save_model == True:
        import joblib

        filePath = '../data_Project_inphb/models_saved/logisticRegression_'
        fileName = 'md_4_bkup_.joblib'

        with open(filePath + fileName, 'wb') as f:
            joblib.dump(lr, f)
    
    coef = pd.DataFrame([list(X.columns), list(lr.coef_[0])])
    
    print(coef)


# Not yet used
def My_model (X, Y, size, RdomState = 42):
    from sklearn.model_selection import train_test_split
    from sklearn import metrics
    from sklearn.linear_model import LogisticRegression
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=size, 
                                                       random_state=RdomState )
    model = LogisticRegression(random_state= RdomState)
    model.fit(X_train, Y_train)
    # Run the model
    Y_pred = model.predict(X_test)
    Y_prob = model.predict_proba(X_test)[:,1]
    score_train = model.score(X_train, Y_train)
    score_test = model.score(X_test, Y_test)
    metric = metrics.classification_report(Y_test, Y_pred)
    
    return {"y_test": Y_test, "prediction": Y_pred, "proba":Y_prob,
           "score_train": score_train, "score_test": score_test,
           "model": model, "metric": print(metric)}
    

# Not yet used
def clf_importances(X, clf):
    import pylab as pl
    import numpy as np
    importances = clf.feature_importances_
    indices = np.argsort(importances)[::-1]
    pl.title("Feature importances")
    for tree in clf.estimators_:
        pl.plot(xrange(X.shape[1]), tree.feature_importances_[indices], "r")
        pl.plot(xrange(X.shape[1]), importances[indices], "b")
        pl.show();
    for f in range(X.shape[1]):
        print("%d. feature: %s (%f)" %(f + 1, X.columns[indices[f]], importances[indices[f]]))


# Not yet used
def parse_model_final(X):
    import pandas as pd
    if "Survived" not in X.columns:
        raise ValueError("target column survived should belong to df")
    target = X["Survived"]
    X['title'] = X['Name'].map(lambda x: x.split(',')[1].split('.')[0])
    X['surname'] = X['Name'].map(lambda x: '(' in x)
    X['Cabin'] = X['Cabin'].map(lambda x: x[0] if not pd.isnull(x) else -1)
    to_dummy = ['Pclass', 'Sex', 'title', 'Embarked', 'Cabin']
    for dum in to_dummy:
        split_temp = pd.get_dummies(X[dum], prefix=dum)
        X = X.join(split_temp)
        del X[dum]
    X['Age'] = X['Age'].fillna(X['Age'].median())
    X['is_child'] = X['Age'] <= 8
    to_del = ["PassengerId", "Name", "Survived", "Ticket"]
    for col in to_del:
        del X[col]
    return X, target  
