.PHONY:  all package push-to-prod clean distclean describe doc qa
.SILENT: all package push-to-prod clean distclean describe doc qa

current_dir = $(notdir $(shell pwd))

all: distclean package

package:
	@echo Building $(current_dir)...
	python setup.py sdist

clean:
	@echo Cleaning $(current_dir)...
	find . -name __pycache__ -print | xargs rm -rf 2>/dev/null
	find . -name '*.pyc' -delete
	rm -rf doc/build/*
	rm -rf qa/*

distclean: clean
	rm -rf dist build *.egg-info

describe:
	@echo $(current_dir) "\t" `git describe --abbrev=0 2>/dev/null || echo 'no tag'`

doc:
	$(MAKE) -C doc html

qa:
	bin/source_analysis.sh
