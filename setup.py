import codecs, os, re
from setuptools import setup, find_packages

with codecs.open('DESCRIPTION.rst', encoding='utf-8') as f:
    long_description = f.read()

here = os.path.abspath(os.path.dirname(__file__))

def find_version(*file_paths):
    with codecs.open(os.path.join(here, *file_paths), 'r', 'latin1') as f:
        version_file = f.read()

    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]", version_file, re.M)
    
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")

setup(
    name = 'data-Project-inphb',
    version = find_version('data_Project_inphb', '__init__.py'),

    install_requires = [
        'numpy>=0.0.0',
        'time>=0.0.0',
        'pandas>=0.0.0',
        'scikit-learn>=0.0.0'
    ],
    
    tests_require = [
        'coverage',
        'rednose',
        'nose',
        'nose-exclude'
    ],

    description = "Common utility functions",
    long_description = long_description,

    license = '',
    author = "Naïss Haïr SAID ALI",
    author_email = "naisshair.officiel@gmail.com",
    url = "https://www.twitter.com/NaissHairOff/",
    download_url = '',
    classifiers = [
        'Development Status :: 1 - Beta',
        'Environment :: Console',
        'Operating System :: OS Independent',
        'Intended Audience :: Science',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.8',
        'Topic :: Engineering',
    ],

    packages = find_packages(),
)
