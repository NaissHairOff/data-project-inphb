import joblib
import pandas as pd

pathName = '../data_Project_inphb/models_saved/'
fileName = 'logisticRegression_md_2_bkup_.joblib'

def predict(SibSp, Parch, Fare):
    """This function predict if or not passenger will probably died or not in Titanic with the probability"""
    
    #We need to deserialize both the model and the encoders used to create them
    model = joblib.load(pathName + fileName)

    val = [SibSp, Parch, Fare]
    X = pd.DataFrame(val).transpose()
    X.columns = ['SibSp','Parch', 'Fare']

    X[["SibSp", "Parch"]] = X[["SibSp", "Parch"]].astype("object")
    X.Fare = X.Fare.astype("double")

    return 'This passenger is likely to die {} with a probability of {:2.2%}'.format(model.predict(X), (1 - model.predict_proba(X)[0][0]))

def hello():
    return "Hello Naïss Haïr SAID ALI and welcome to this project"