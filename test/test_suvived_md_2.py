import pandas as pd

import sys
sys.path.insert(1, 
    './data-project-inphb/data_Project_inphb/')
from preprocessing.preprocessing import convert_columns

#We will need to deserialize both the model and the encoders used to create them
import joblib
pathName = './data-project-inphb/data_Project_inphb/models_saved/'
fileName = 'logisticRegression_md_2_bkup_.joblib'

titanic = pd.read_csv("./data-project-inphb/data/train.csv", sep = ',')
titanic = titanic.loc[1:1, ['SibSp', 'Parch', 'Fare', 'Survived']] 

model = joblib.load(pathName + fileName)

categorical_col = ["Survived", "SibSp", "Parch"]

numerical_col  = [var for var in titanic.columns if var not in categorical_col]

titanic = convert_columns(titanic, categorical_col, numerical_col)

X = titanic[['SibSp', 'Parch', 'Fare']]

print(titanic['Survived'])
print(model.predict(X))
print(1 - model.predict_proba(X)[0][0])